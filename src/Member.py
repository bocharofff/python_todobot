from src.ToDo_Journal import *
from src.UsersList import *
import datetime


class Member:
    """
        Класс, представляющий отдельного пользователя.

        Attributes:
            usersList (UsersList): ID пользователя
            todoJournal (TodoJournal): Список задач пользователя.
        """

    def __init__(self, user_id: int, username, first_name, last_name) -> None:
        """
        Конструктор класса Member.

        Parameters:
            user_id (int):
        """
        self.user_id = user_id

        if not os.path.exists(f"./data/users/{self.user_id}"):
            os.makedirs(f"./data/users/{self.user_id}")

        self.todoJournal = TodoJournal(f"./data/users/{self.user_id}/tasks.json")
        self.usersList = UsersList(f"./data/users/{self.user_id}/users.json")

        self.usersList.add_user(user_id, username, first_name, last_name)

    def set_as_closed_task(self, index: int) -> None:
        """
        Отмечает задачу как выполненную, то есть удаляет ее из списка задач пользователя
        и перемещает в общий файл с выполненными задачами.

        Parameters:
            index (int): Индекс задачи, которую нужно отметить как выполненную.
        """
        self.todoJournal.set_as_closed_task(index)

    def ask_help(self, task_index):
        pass

    def add_task(self, user_id, task_text, task_date):
        index = self.usersList.find_user_by_id(user_id)

        if index is None:
            return -1
        else:
            task_date = task_date.split('-')
            task = Task(task_text, datetime.date(int(task_date[0]), int(task_date[1]), int(task_date[2])))

            user_data = self.usersList[index]
            Member(user_data.user_id, user_data.username, user_data.first_name, user_data.last_name).todoJournal.add_task(task)

            return 0

    def reassign_task(self, first_id, second_id, task_index):
        index1 = self.usersList.find_user_by_id(first_id)
        index2 = self.usersList.find_user_by_id(second_id)

        if index1 is None or index2 is None:
            return -1

        first_user_data = self.usersList[index1]
        second_user_data = self.usersList[index2]

        first_user = Member(first_user_data.user_id, first_user_data.username, first_user_data.first_name, first_user_data.last_name)
        second_user = Member(second_user_data.user_id, second_user_data.username, second_user_data.first_name, second_user_data.last_name)

        second_user.add_task(second_user.user_id, first_user.todoJournal[task_index].task_content, first_user.todoJournal[task_index].execution_date)
        first_user.set_as_closed_task(task_index)

    def add_user(self, user_id: int, username, first_name, last_name) -> int:
        if self.usersList.find_user_by_id(user_id) is not None:
            return -1
        else:
            self.usersList.add_user(user_id, username, first_name, last_name)
            return 0

    def delete_user(self, user_id: int):
        if self.usersList.find_user_by_id(user_id) is None:
            return -1
        else:
            self.usersList.delete_user(user_id)
            return 0




