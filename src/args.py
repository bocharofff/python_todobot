import argparse


def parse_args():
    parser = argparse.ArgumentParser()
    composing = parser.add_argument_group("Создание бота", "Чтобы создать бота, необходимо выполнить -b")
    composing.add_argument(
        "-b",
        "--bot",
        dest="bot",
        metavar="token",
        nargs=1,
        help="Создает нового бота по токену",
        default="7350715226:AAFAXe2BASwxOoJYTWj3DlZZyESTNpN5nNI"
    )

    return parser.parse_args()
