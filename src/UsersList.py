import sys
import os
import json


#TODO docccccc
class User:
    """
    Вспомогательный класс для UsersList

    Parameters:
        user_id (int): Путь до файла с задачами.
    """
    def __init__(self, user_id, username, first_name, last_name):
        self.user_id = user_id
        self.username = username
        self.first_name = first_name
        self.last_name = last_name

    def __str__(self) -> str:
        return f'- user_id: {self.user_id}\n  username: {self.username}\n  {self.first_name} {self.last_name}'


class UsersList:
    def __init__(self, path_to_users: str) -> None:
        """
        Конструктор класса TodoJournal.
        Файл считывается при создании объекта класса, если файла нет, то он создаётся.

        Parameters:
            path_to_users (str): Путь до файла с задачами.
        """
        self.path_to_users = path_to_users
        self.users = []

        if not os.path.exists(path_to_users):
            file = open(path_to_users, 'w')
            file.write('[]')
            file.close()
        else:
            self.read_json()

    def add_user(self, user_id, username, first_name, last_name) -> None:
        """
        Добавляет нового пользователя.

        Parameters:
            last_name:
            first_name:
            username:
            user_id:
        """
        if self.find_user_by_id(user_id) is None:
            self.users.append(User(user_id, username, first_name, last_name))
            self.update()

    def find_user_by_id(self, user_id: int):
        index = 0
        for user in self.users:
            if user.user_id == user_id:
                return index

            index += 1

    def find_user_by_username(self, username: str):
        index = 0
        for user in self.users:
            if user.username == username:
                return index

            index += 1

    def delete_user(self, user_id: int) -> int:
        index = self.find_user_by_id(user_id)
        if index is not None:
            self.users.remove(self.users[index])
            self.update()
            return 0
        else:
            return 1

    def read_json(self) -> None:
        with open(self.path_to_users, 'r') as json_file:
            users_before_handling = json.load(json_file)

        for user in users_before_handling:
            self.users.append(User(user['user_id'], user['username'], user['first_name'], user['last_name']))

    def toJSON(self) -> list:

        json_users = []
        for user in self.users:
            json_users.append({"user_id": user.user_id, "username": user.username, "first_name": user.first_name, "last_name": user.last_name})

        return json_users

    def update(self) -> None:
        data = self.toJSON()
        with open(self.path_to_users, "w", encoding='utf-8') as todo_file:
            json.dump(
                data,
                todo_file,
                sort_keys=True,
                indent=4,
                ensure_ascii=False,
            )

    def __len__(self) -> int:
        return len(self.users)

    def __iter__(self):
        return iter(self.users)

    def __getitem__(self, index: int) -> User:
        if 0 <= index < len(self.users):
            return self.users[index]
        else:
            raise IndexError

    def __str__(self):
        if len(self.users) <= 1:
            return "Нет пользователей"

        output = ""
        count = 0

        for user in self.users:
            count += 1
            if count == 1:
                continue
            output += f"№ {str(count - 1)}\n{str(user)}\n\n"
        return output
