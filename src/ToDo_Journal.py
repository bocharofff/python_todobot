import json
import sys
import os
import datetime


class Task:
    """
    Класс, представляющий отдельную тудушку.

    Attributes:
        task_content (str): Содержание задачи.
        execution_date (datatime.date): Срок выполнения задачи.
    """

    def __init__(self, task_content: str, execution_date: datetime.date) -> None:
        """
        Конструктор класса Task.

        Parameters:
            task_content (str): Содержание задачи.
            execution_date (datatime.date): Срок выполнения задачи.
        """
        self.task_content = task_content
        self.execution_date = execution_date

    def __str__(self) -> str:
        """
        Вывод информации о задаче.

        Returns:
            str: Содержание задачи и сроки ее выполнения.
        """
        return f'- задача: {self.task_content}\n  выполнить до: {self.execution_date}'


class TodoJournal:
    """
    Класс, представляющий список задач (создаётся для каждого пользователя).

    Attributes:
        path_to_tasks (str): Путь до файла с тудушками.
        tasks (list): Список тудушек - объектов класса Task.
    """

    def __init__(self, path_to_tasks: str) -> None:
        """
        Конструктор класса TodoJournal.
        Файл считывается при создании объекта класса, если файла нет, то он создаётся.

        Parameters:
            path_to_tasks (str): Путь до файла с задачами.
        """
        self.path_to_tasks = path_to_tasks
        self.tasks = []

        if not os.path.exists(path_to_tasks):
            file = open(path_to_tasks, 'w')
            file.write('[]')
            file.close()
        else:
            self.read_json()

    def __len__(self) -> int:
        return len(self.tasks)

    def __iter__(self):
        return iter(self.tasks)

    def __getitem__(self, index: int) -> Task:
        if 0 <= index < len(self.tasks):
            return self.tasks[index]
        else:
            raise IndexError

    def __str__(self):
        if len(self.tasks) == 0:
            return "Нет задач"

        output = ""
        count = 1

        for task in self.tasks:
            output += f"№ {str(count)}\n{str(task)}\n\n"
            count += 1

        return output

    def read_json(self) -> None:
        """
        Считывает задачи из файла JSON в список.
        """
        try:
            with open(self.path_to_tasks, 'r') as json_file:
                tasks_before_handling = json.load(json_file)

            for task in tasks_before_handling:
                self.tasks.append(Task(task['task_content'], task['execution_date']))

        except FileNotFoundError as error:
            print(f"{error}")
            print(f"Не существует такого файла: {self.path_to_tasks}")
            sys.exit(1)

    def add_task(self, task: Task) -> None:
        """
        Добавляет новую тудушку.

        Parameters:
            task (Task): Новая задача.
        """
        self.tasks.append(task)
        self.update()

    def set_as_closed_task(self, index: int) -> None:
        """
        Отмечает задачу как выполненную, то есть удаляет ее из списка задач пользователя
        и перемещает в общий файл с выполненными задачами.

        Parameters:
            index (int): Индекс задачи, которую нужно отметить как выполненную.
        """
        try:
            self.tasks.remove(self.tasks[index])
            self.update()

        except IndexError as error:
            print(f"{error}")
            print(f"Не cуществует такого индекса: {index}")

    def toJSON(self) -> list:
        """
        Подготавливает тудушки для записи в файл.

        Returns:
            list: список словарей с задачами (в формате JSON).
        """
        json_tasks = []
        for task in self.tasks:
            json_tasks.append({"task_content": task.task_content, "execution_date": str(task.execution_date)})

        return json_tasks

    def update(self) -> None:
        """
        Записывает актуальный список задач в файл.
        """
        try:
            data = self.toJSON()
            with open(self.path_to_tasks, "w", encoding='utf-8') as todo_file:
                json.dump(
                    data,
                    todo_file,
                    sort_keys=True,
                    indent=4,
                    ensure_ascii=False,
                )
        except FileNotFoundError as error:
            print(f"{error}")
            print(f"Не существует такого файла: {self.path_to_tasks}")
            sys.exit(1)
