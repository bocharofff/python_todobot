import os
import time
import telebot
from telebot import types
from src.args import parse_args
from src.Member import Member
from src.UsersList import UsersList

PATH_TO_USERS_ID = "./data/users_id.json"

args = parse_args()
BOT = telebot.TeleBot(args.bot)

if not os.path.exists("./data"):
    os.makedirs("./data")

if not os.path.exists("./data/users"):
    os.makedirs("./data/users")

if not os.path.exists(PATH_TO_USERS_ID):
    file = open(PATH_TO_USERS_ID, 'w')
    file.write('[]')
    file.close()

USERS = UsersList(PATH_TO_USERS_ID)


def create_user(message):
    return Member(message.from_user.id, message.from_user.username, message.from_user.first_name, message.from_user.last_name)


@BOT.message_handler(commands=['start'])
def hello(message):
    if USERS.find_user_by_id(message.from_user.id) is None:
        USERS.add_user(message.from_user.id, message.from_user.username, message.from_user.first_name, message.from_user.last_name)

    create_user(message)

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    btn_get_users = types.KeyboardButton('Вывести пользователей')
    btn_add_user = types.KeyboardButton('Добавить пользователя')
    btn_delete_user = types.KeyboardButton('Удалить пользователя')
    btn_add_task = types.KeyboardButton('Поставить задачу')
    btn_get_tasks = types.KeyboardButton('Пoсмотреть задачи')
    btn_close_task = types.KeyboardButton('Закрыть задачу')
    btn_reass_task = types.KeyboardButton('Переназначить задачу')

    markup.row(btn_add_user, btn_delete_user, btn_get_users)
    markup.row(btn_get_tasks, btn_close_task, btn_add_task, btn_reass_task)

    BOT.send_message(message.chat.id, f'Привет, {message.from_user.first_name}!', reply_markup=markup)
    BOT.register_next_step_handler(message, on_click)


def on_click(message):
    time.sleep(1)
    if message.text == "Вывести пользователей":
        get_users(message)
    elif message.text == "Добавить пользователя":
        add_user(message)
    elif message.text == "Удалить пользователя":
        delete_user(message)
    elif message.text == "Поставить задачу":
        add_task(message)
    elif message.text == "Пoсмотреть задачи":
        get_tasks(message)
    elif message.text == "Закрыть задачу":
        close_task(message)
    elif message.text == "Переназначить задачу":
        reassign_task(message)


def add_user(message):
    BOT.send_message(message.chat.id, 'Напиши @username пользователя или "отмена"')

    @BOT.message_handler(content_types=['text'])
    def get_username(message):
        if message.text == "отмена":
            BOT.register_next_step_handler(message, on_click)
            BOT.send_message(message.chat.id, 'Выбери действие')
            return

        index = USERS.find_user_by_username(message.text[1:])
        if index is not None:
            subuser = USERS[index]
            if create_user(message).add_user(subuser.user_id, subuser.username, subuser.first_name, subuser.last_name) == 0:
                BOT.send_message(message.chat.id, 'Пользователь успешно добавлен!')
                BOT.register_next_step_handler(message, on_click)
            else:
                BOT.send_message(message.chat.id, 'Такой пользователь уже есть')
                BOT.register_next_step_handler(message, on_click)
        else:
            BOT.send_message(message.chat.id, 'Такой пользователь еще не зарегестрирован')
            add_user(message)


def delete_user(message):
    users = create_user(message).usersList
    if len(users) == 1:
        BOT.send_message(message.chat.id, "Нет пользователей")
        BOT.register_next_step_handler(message, on_click, )
        return

    markup = types.InlineKeyboardMarkup()
    count = -1
    for user in users:
        count += 1
        if count == 0:
            continue

        markup.row(
            types.InlineKeyboardButton(f"{count}. {user.username}", callback_data=f'delete user|{user.user_id}'))

    BOT.send_message(message.chat.id, "Выберите пользователя", reply_markup=markup)

    @BOT.callback_query_handler(func=lambda callback: callback.data.split('|')[0] == 'delete user')
    def callback_message(callback):
        time.sleep(2)
        create_user(message).delete_user(int(callback.data.split('|')[1]))
        BOT.send_message(callback.message.chat.id, "Пользователь удален")
        BOT.register_next_step_handler(callback.message, on_click)


def get_users(message):
    users = create_user(message).usersList
    if len(users) == 1:
        BOT.send_message(message.chat.id, "Нет пользователей")
        BOT.register_next_step_handler(message, on_click)
        return

    markup = types.InlineKeyboardMarkup()
    count = -1
    for user in users:
        count += 1
        if count == 0:
            continue

        markup.row(
            types.InlineKeyboardButton(f"{count}. {user.username}", callback_data=f'show user|{user.user_id}'))

    BOT.send_message(message.chat.id, "Выберите пользователя, чтобы посмотреть его задачи", reply_markup=markup)

    @BOT.callback_query_handler(func=lambda callback: callback.data.split('|')[0] == 'show user')
    def callback_message(callback):
        time.sleep(2)
        user = create_user(callback)
        slave_data = user.usersList[user.usersList.find_user_by_id(int(callback.data.split('|')[1]))]
        slave = Member(slave_data.user_id, slave_data.username, slave_data.first_name, slave_data.last_name)
        BOT.send_message(callback.message.chat.id, f"Задачи @{slave_data.username}:\n{str(slave.todoJournal)}")
        BOT.edit_message_reply_markup(callback.message.chat.id, callback.message.message_id, reply_markup=None)
        BOT.register_next_step_handler(callback.message, on_click)


def add_task(message):
    markup = types.InlineKeyboardMarkup()
    markup.row(types.InlineKeyboardButton("Себе", callback_data=f'add task|{0}'))

    count = 0
    member = create_user(message)
    for sub_user in member.usersList:
        count += 1
        if count == 1:
            continue

        markup.row(types.InlineKeyboardButton(f"{count - 1}. {sub_user.username}", callback_data=f'add task|{str(count - 1)}'))

    BOT.send_message(message.chat.id, 'Выбери пользователя', reply_markup=markup)

    @BOT.callback_query_handler(func=lambda callback: callback.data.split('|')[0] == 'add task')
    def callback_users(callback):
        subuser_id = create_user(message).usersList[int(callback.data.split('|')[1])].user_id
        BOT.register_next_step_handler(callback.message, get_text, subuser_id)
        BOT.send_message(message.chat.id, 'Напиши текст задачи или "отмена"')

    def get_text(message, subuser_id):
        task_text = message.text

        if task_text == "отмена":
            BOT.register_next_step_handler(message, on_click)
            BOT.send_message(message.chat.id, 'Выбери действие')
            return

        BOT.send_message(message.chat.id, 'Напиши срок задачи в формате YYYY-MM-DD или "отмена"')
        BOT.register_next_step_handler(message, get_date, subuser_id, task_text)

    def get_date(message, subuser_id, task_text):
        try:
            task_date = message.text
            if task_date == "отмена":
                BOT.register_next_step_handler(message, on_click)
                BOT.send_message(message.chat.id, 'Выбери действие')
                return

            if create_user(message).add_task(subuser_id, task_text, task_date) == -1:
                BOT.send_message(message.chat.id, 'Пользователь отсутствует, попробуйте еще раз')
                add_task(message)
            else:
                BOT.send_message(message.chat.id, 'Задача успешно добавлена!')
                BOT.send_message(subuser_id, f'Новая задача: {task_text}\nСрок до: {task_date}')
                BOT.register_next_step_handler(message, on_click)

        except ValueError:
            BOT.send_message(message.chat.id, 'Неверный формат даты, попробуй еще раз')
            add_task(message)


def reassign_task(message):
    markup = types.InlineKeyboardMarkup()
    markup.row(types.InlineKeyboardButton("Я", callback_data=f'reass user1|{0}'))

    count = 0
    member = create_user(message)
    for sub_user in member.usersList:
        count += 1
        if count == 1:
            continue

        markup.row(types.InlineKeyboardButton(f"{count - 1}. {sub_user.username}", callback_data=f'reass user1|{str(count - 1)}'))

    BOT.send_message(message.chat.id, 'Выбери пользователя, чья задача переназначается', reply_markup=markup)

    @BOT.callback_query_handler(func=lambda callback: callback.data.split('|')[0] == 'reass user1')
    def callback_user_reass_from(callback):
        subuser_data = create_user(callback).usersList[int(callback.data.split('|')[1])]
        sub_user_id = subuser_data.user_id
        BOT.edit_message_reply_markup(callback.message.chat.id, callback.message.message_id, reply_markup=None)

        #
        subuser = Member(subuser_data.user_id, subuser_data.username, subuser_data.first_name, subuser_data.last_name)
        tasks = subuser.todoJournal
        if len(tasks) == 0:
            BOT.send_message(message.chat.id, "Нет задач")
            BOT.register_next_step_handler(message, on_click)
            return

        markup = types.InlineKeyboardMarkup()
        count = 0
        for task in tasks:
            count += 1
            markup.row(types.InlineKeyboardButton(f"{count}. {task.task_content}", callback_data=f'reass task|{str(count - 1)}|{sub_user_id}'))

        BOT.send_message(message.chat.id, "Выберите задачу", reply_markup=markup)

        @BOT.callback_query_handler(func=lambda callback: callback.data.split('|')[0] == 'reass task')
        def callback_index_reass(callback):
            time.sleep(2)
            data = int(callback.data.split('|')[1])
            BOT.edit_message_reply_markup(callback.message.chat.id, callback.message.message_id, reply_markup=None)

            markup = types.InlineKeyboardMarkup()
            markup.row(types.InlineKeyboardButton("Я", callback_data=f"reass user2|{0}|{int(callback.data.split('|')[2])}"))

            count = 0
            member = create_user(message)
            for sub_user in member.usersList:
                count += 1
                if count == 1:
                    continue

                markup.row(
                    types.InlineKeyboardButton(f"{count - 1}. {sub_user.username}",
                                               callback_data=f"reass user2|{str(count - 1)}|{int(callback.data.split('|')[2])}"))

            BOT.send_message(message.chat.id, 'Выбери пользователя, кому передать задачу', reply_markup=markup)

            @BOT.callback_query_handler(func=lambda callback: callback.data.split('|')[0] == 'reass user2')
            def callback_user_reass_to(callback):
                subuser_id_to = create_user(callback).usersList[int(callback.data.split('|')[1])].user_id
                BOT.edit_message_reply_markup(callback.message.chat.id, callback.message.message_id, reply_markup=None)

                subuser_id_from = int(callback.data.split('|')[2])
                if subuser_id_from != subuser_id_to:
                    create_user(message).reassign_task(subuser_id_from, subuser_id_to, data)
                    BOT.send_message(message.chat.id, 'Задача успешно переназначена!')

                    BOT.send_message(subuser_id_to, f'Переназначена новая задача')
                    BOT.register_next_step_handler(message, on_click)
                else:
                    BOT.send_message(message.chat.id, 'Нельзя переназначать себе же!!!!')
                    BOT.register_next_step_handler(message, on_click)


def get_tasks(message):
    BOT.send_message(message.chat.id, str(create_user(message).todoJournal))
    BOT.register_next_step_handler(message, on_click)


def close_task(message):
    tasks = create_user(message).todoJournal
    if len(tasks) == 0:
        BOT.send_message(message.chat.id, "Нет задач")
        BOT.register_next_step_handler(message, on_click,)
        return

    markup = types.InlineKeyboardMarkup()
    count = 0
    for task in tasks:
        count += 1
        markup.row(types.InlineKeyboardButton(f"{count}. {task.task_content}", callback_data=f'delete task|{str(count - 1)}'))

    BOT.send_message(message.chat.id, "Выберите задачу", reply_markup=markup)

    @BOT.callback_query_handler(func=lambda callback: callback.data.split('|')[0] == 'delete task')
    def callback_message(callback):
        time.sleep(2)
        create_user(message).set_as_closed_task(int(callback.data.split('|')[1]))
        BOT.edit_message_reply_markup(callback.message.chat.id, callback.message.message_id, reply_markup=None)

        BOT.send_message(callback.message.chat.id, "Задача закрыта")
        BOT.register_next_step_handler(callback.message, on_click)


BOT.infinity_polling(none_stop=True)
